package model;

public enum Direction {

    /**
     * possible movement and texture direction.
     */
    UP, RIGHT, LEFT, DOWN

}
