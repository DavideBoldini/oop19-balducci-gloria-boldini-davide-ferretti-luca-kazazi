package model.updateLevel;

public interface UpdateLevelInterface {

    /**
     * method to refresh level entities status.
     */
    void tick();

}
